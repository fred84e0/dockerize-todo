from django.db import IntegrityError
from django.shortcuts import render, reverse
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from .models import PasswordResetRequest


def login(request):
    context = {}

    if request.method == 'POST':
        user = authenticate(
            request,
            username=request.POST['user'],
            password=request.POST['password'])
        if user:
            dj_login(request, user)
            return HttpResponseRedirect(reverse('todo_app:index'))
        else:
            context = {
                'error': 'Bad username or password.'
            }

    return render(request, 'login_app/login.html', context)


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('login_app:login'))


def request_password_reset(request):
    if request.method == "POST":
        if username := request.POST.get('username'):
            user = User.objects.get(username=username)
            prr = PasswordResetRequest()
            # TODO: handle user not found
            prr.user = user
            prr.save()
            print(prr)
            return HttpResponseRedirect(reverse('login_app:password_reset'))
        # TODO: email provided, not user name
    return render(request, 'login_app/request_password_reset.html')


def password_reset(request):
    if request.method == "POST":
        if token := request.POST.get('token'):
            # TODO: what if token does not exist?
            prr = PasswordResetRequest.objects.get(token=token)
            if request.POST.get('password') == request.POST.get('confirm_password'):
                user = prr.user
                user.set_password(request.POST.get('password'))
                user.save()
                return HttpResponseRedirect(reverse('login_app:login'))
    return render(request, 'login_app/password_reset.html')


def sign_up(request):
    context = {}

    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        user_name = request.POST['user']
        email = request.POST['email']

        if password == confirm_password:
            try:
                User.objects.create_user(user_name, email, password)
                return HttpResponseRedirect(reverse('login_app:login'))
            except IntegrityError:
                context = {
                    'error': 'User name is already taken - please try a different user name.'
                }
        else:
            context = {
                'error': 'Passwords did not match. Please try again.'
            }

    return render(request, 'login_app/sign_up.html', context)


def delete_account(request):
    ...
