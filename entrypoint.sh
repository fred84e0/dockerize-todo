#!/bin/sh
echo "** RTE Mode: $RTE "
echo "RUNNING"
python manage.py check 
python manage.py makemigrations 
python manage.py migrate

case "$RTE" in
    dev )
        echo "** Development mode."
        coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        coverage report -m
        python manage.py runserver 0.0.0.0:8080
        ;;
    test )
        echo "** Test mode."
        coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        coverage report -m --fail-under=50
        ;;
    prod )
        echo "** Production mode."
        python manage.py check --deploy
        ;;
esac