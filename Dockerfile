# FROM python:3
# ENV PYTHONDONTWRITEBYTECODE=1
# ENV PYTHONUNBUFFERED=1

# WORKDIR /todo
# COPY requirements.txt /todo
# RUN pip install --upgrade pip && pip install -r requirements.txt
# RUN pip install psycopg2
# COPY . /todo

# COPY ./entrypoint.sh /
# ENTRYPOINT ["sh", "/entrypoint.sh"]

FROM python:3.10-alpine

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN apk add python3-dev postgresql-client postgresql-dev musl-dev build-base

COPY . /todo
WORKDIR /todo

RUN pip install --upgrade pip && pip install -r requirements.txt
COPY ./entrypoint.sh /
ENTRYPOINT ["sh", "/entrypoint.sh"]