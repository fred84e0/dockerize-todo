from django.test import TestCase
from .models import Todo, User


class TodoTestCase(TestCase):

    def setUp(self):
        print('setUp the tests')
        self.user = User.objects.create(username='testuser')
        self.user.set_password('password')
        self.user.save()

    def test_create_todo(self):
        todo = Todo()
        todo.user = self.user
        todo.text = "Test my list"
        todo.status = False
        todo.save()

    def test_delete_todo(self):
        todo = Todo.objects.filter(text="Test my list")
        todo.delete()
